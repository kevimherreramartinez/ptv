<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$ids=1;
		$_SESSION['suc']=1;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">	
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	<link rel="stylesheet" type="text/css" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos5.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	 <div id="form2">
    	<div id="w100lbt">
    		Listado de productos desactivados
    	</div>
    </div>
   
    <div id="tablaw100">
     <div id="agregar"> <a href="listarPro.php?idPro=1">Activados</a></div>
		<form method="POST" action="controler/agrExi.php">
			<input type="hidden" value="<?php echo $ids ?>" name="idSucursal2">
    	<?php 
    	$sumaSuc=0;
    	$sumaSuc2=0;
    	$sumaTot=0;
    	$sumaInput=1;
    	
    		include("controler/connect_db.php");
    		$prow=mysqli_query($link,"SELECT * FROM sucursales where idSucursal='$ids'  ");
    		while ($prodw=mysqli_fetch_array($prow)) {
    			echo "<div id='w100lbt'> SUCURSAL ".$prodw[0]." , ".$prodw[1]." ".$prodw[2]."</div>";
    			echo      '
					<div id="indicew100">
			    		<div class="indiceNumeros">
			    			#
			    		</div>
			    		<div class="indiceMedioLargo">
			    			Nombre
			    		</div>
			    		<div class="indiceClave">
			    			Clave
			    		</div>
			    		<div class="indiceClave">
			    			Compra
			    		</div>
			    		<div class="indiceClave">
			    			Venta
			    		</div>
			    	
			    	</div>';
	    		$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND status=0 order by nombre   ");
	    	//	$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND idProducto='$id_producto' ");
		    				$contador=1;
		    		while ($prod=mysqli_fetch_array($pro)) {
		    			if ($prod[7]>1 ) {
		    				$cs="filaB";
		    			}
		    			if ($prod[7]==1 ) {
		    				$cs="filaBG";
		    			}
		    			if ($prod[7]<1 ) {
		    				$cs="filaBR";
		    			}
		    			$faltante=$prod[10]-$prod[7];
		    			if ($faltante<0) {
		    				$faltante=0;
		    			}
		    			echo '
		    			<div id="fila" class="'.$cs.'" >
				    			<div class="indiceNumeros">
							    			'.$contador.'
							    		</div>
					    		<div class="filaMedioLargo">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="filaClave">
					    			'.$prod[6] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[3] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[2] .'
					    		</div>
						
					    		<div class="filaNumeros">
					    				<a href="controler/ap.php?idPro='.$prod[1] .'">Activar</a>
					    		</div>
					    </div>';
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
					    	$sumaSuc2=$sumaSuc2+($prod[7]*$prod[2]);
					    	$contador=$contador+1;
		    		}
		    		 echo'		
		    		 			
						    <div id="w100lbt">
						    
						    </div>';
						    $sumaTot=$sumaTot+$sumaSuc;
						    $sumaSuc=0;
						   
						 
					
    				
						  
		    	}
				   
		    	 ?>
								    </form>
    </div>

<div id="openModal2" class="modalDialog2">
  <div >
	<a href="#close2" title="Close2" class="close2">X</a>
    <div id="formModal">
    	<div id="tituloModal">
    		Producto nuevo
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insPro.php">
    		<input type="hidden" name="idSuc" value="<?php echo $idSuc ?>"></br>
    		<input type="text" name="clave" class="ipt-iModal" placeholder="Clave" autocomplete="off"  value="0">
    		<input type="text" name="nom" class="ipt-iModal" placeholder="Nombre" autocomplete="off" required autofocus="">
    		<input type="number" name="ven" step="any" class="ipt-iModal" placeholder="Precio Venta" autocomplete="off" step="0.01" required>
    		<input type="number" name="com" step="any" class="ipt-iModal" placeholder="Precio Compra" autocomplete="off" step="0.01" required>
            <input type="number" name="can" step="any" class="ipt-iModal" placeholder="Cantidad" autocomplete="off" step="0.01" required value="0">
             <input type="number" name="sto" step="any" class="ipt-iModal" placeholder="Stock Minimo" autocomplete="off" step="0.01" required value="0">
            <input type="hidden" name="mis" step="any" class="ipt-iModal" placeholder="Comision" value="0" autocomplete="off" step="0.01" required>
    		<textarea name="des" class="ipt-iModal" cols="30" rows="5" placeholder="Caracteristicas" value="maxima calidad" required>maxima calidad</textarea>
    		<input type="submit" class="btn-iModal" value="Guardar">
    	</form>
    </div>
  </div>
</div>
   
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>