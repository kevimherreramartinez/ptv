<script>
    /**
 * Una clase para interactuar con el plugin
 * 
 * @author parzibyte
 * @see https://parzibyte.me/blog
 */
const C = {
    AccionWrite: "write",
    AccionCut: "cut",
    AccionCash: "cash",
    AccionCutPartial: "cutpartial",
    AccionAlign: "align",
    AccionFontSize: "fontsize",
    AccionFont: "font",
    AccionEmphasize: "emphasize",
    AccionFeed: "feed",
    AccionQr: "qr",
    AlineacionCentro: "center",
    AlineacionDerecha: "right",
    AlineacionIzquierda: "left",
    FuenteA: "A",
    FuenteB: "B"
};

const URL_PLUGIN = "http://localhost:8000";

class OperacionTicket {
    constructor(accion, datos) {
        this.accion = accion + "";
        this.datos = datos + "";
    }
}
class Impresora {
    constructor(ruta) {
        if (!ruta) ruta = URL_PLUGIN;
        this.ruta = ruta;
        this.operaciones = [];
    }

    static setImpresora(nombreImpresora, ruta) {
        if (ruta) URL_PLUGIN = ruta;
        return fetch(URL_PLUGIN + "/impresora", {
                method: "PUT",
                body: JSON.stringify(nombreImpresora),
            })
            .then(r => r.json())
            .then(respuestaDecodificada => respuestaDecodificada === nombreImpresora);
    }

    static getImpresora(ruta) {
        if (ruta) URL_PLUGIN = ruta;
        return fetch(URL_PLUGIN + "/impresora")
            .then(r => r.json());
    }

    static getImpresoras(ruta) {
        if (ruta) URL_PLUGIN = ruta;
        return fetch(URL_PLUGIN + "/impresoras")
            .then(r => r.json());
    }

    cut() {
        this.operaciones.push(new OperacionTicket(C.AccionCut, ""));
    }

    cash() {
        this.operaciones.push(new OperacionTicket(C.AccionCash, ""));
    }

    cutPartial() {
        this.operaciones.push(new OperacionTicket(C.AccionCutPartial, ""));
    }

    setFontSize(a, b) {
        this.operaciones.push(new OperacionTicket(C.AccionFontSize, `${a},${b}`));
    }

    setFont(font) {
        if (font !== C.FuenteA && font !== C.FuenteB) throw Error("Fuente inválida");
        this.operaciones.push(new OperacionTicket(C.AccionFont, font));
    }
    setEmphasize(val) {
        if (isNaN(parseInt(val)) || parseInt(val) < 0) throw Error("Valor inválido");
        this.operaciones.push(new OperacionTicket(C.AccionEmphasize, val));
    }
    setAlign(align) {
        if (align !== C.AlineacionCentro && align !== C.AlineacionDerecha && align !== C.AlineacionIzquierda) {
            throw Error(`Alineación ${align} inválida`);
        }
        this.operaciones.push(new OperacionTicket(C.AccionAlign, align));
    }

    write(text) {
        this.operaciones.push(new OperacionTicket(C.AccionWrite, text));
    }

    feed(n) {
        if (!parseInt(n) || parseInt(n) < 0) {
            throw Error("Valor para feed inválido");
        }
        this.operaciones.push(new OperacionTicket(C.AccionFeed, n));
    }

    end() {
        return fetch(this.ruta + "/imprimir", {
                method: "POST",
                body: JSON.stringify(this.operaciones),
            })
            .then(r => r.json());
    }

    imprimirEnImpresora(nombreImpresora) {
        const payload = {
            operaciones: this.operaciones,
            impresora: nombreImpresora,
        };
        return fetch(this.ruta + "/imprimir_en", {
                method: "POST",
                body: JSON.stringify(payload),
            })
            .then(r => r.json());
    }

    qr(contenido) {
        this.operaciones.push(new OperacionTicket(C.AccionQr, contenido));
    }

}
</script>
<?php 
		require("connect_db.php");
$fecha=date('Y-m-d H:i:s');
	session_start();
	ob_start(); 
	$c=$_SESSION['c'];
	$idSuc=$_SESSION['suc'];
	$idTrabajador=$_POST['idTrabajador'];
	$i=$_SESSION['i'];
	$p=$_SESSION['p'];
	$a=$_SESSION['a'];
	$t=$_SESSION['t'];
	$o=$_SESSION['o'];
	$numeroProcductos=count($c);
	$to=$_POST['to'];
	$ba=$_POST['ba'];
	$monto=0;
	$fi=$_POST['fi'];
	$ficha2="INSERT INTO venta SET   total='$to' , estado=4 , idSucursal='$idSuc' ,  idTrabajador='$idTrabajador' , fecha='$fecha' ";
	$ejecutar_insertar_ficha2=mysqli_query($link , $ficha2);
	$idVenta=mysqli_insert_id($link);

		$suv=mysqli_query($link , "SELECT * FROM trabajador where idTrabajador='$idTrabajador'");
				$suvvv=mysqli_fetch_array($suv);
			$arregloP=0;
			$cosd=1;
			for ($ii=1; $ii <=$numeroProcductos ; $ii++) { 
				$ficha3="INSERT INTO pedido SET  idVenta='$idVenta' , 
				idProducto='$i[$ii]' , cantidad='$a[$ii]' ,
				  total='$t[$ii]' ,tipo='$o[$ii]' ";
				$ejecutar_insertar_ficha3=mysqli_query($link , $ficha3);


				if ($o[$ii]==2) { // ve si el pedido es kit 
					$fetch2 = mysqli_query($link,"SELECT * FROM kit where idKit='$i[$ii]' "); 
					$row2 = mysqli_fetch_array($fetch2);
					$a[$ii]=$row2[3]*$a[$ii];
					$i[$ii]=$row2[2];
				}
				else{
					$fetch2 = mysqli_query($link,"SELECT * FROM producto where idProducto='$i[$ii]' "); 
					$row2 = mysqli_fetch_array($fetch2);
				}


				$fetch = mysqli_query($link,"SELECT * FROM producto where idProducto='$i[$ii]' "); 
				$row = mysqli_fetch_array($fetch);

				$apac[$arregloP]= $cosd.".- ".$row2[1]." (".$a[$ii].")";
				$arregloP=$arregloP+1;
				$cosd=$cosd+1;


				$row[7]=$row[7]-$a[$ii];

				$insertar1="UPDATE producto set almacen='$row[7] '	where idProducto='$i[$ii] '";
			   	$ejecutar_insertar1=mysqli_query($link,$insertar1);

			}
			

				$_SESSION['c2']=$c;
				$_SESSION['a2']=$a;
				$_SESSION['t2']=$t;
				$_SESSION['pago']=$to;
				$_SESSION['cambio']=$fi;
				$_SESSION['cli']=$ba;
			
				//
			//echo "<script>window.open('../pdf2/index.php', '_blank');</script>";
?>

<script>
let impresora = new Impresora();
impresora.write("Trabajador: <?php echo $suvvv[1] ?>\n");
impresora.write("Fecha :<?php echo $fecha ?>\n");
<?php for($iiii=0; $iiii <$arregloP ; $iiii++){ ?>
	impresora.write(" <?php echo $apac[$iiii] ?>\n");
<?php } ?>
impresora.end()
    .then(valor => {
        loguear("Al imprimir: " + valor);
    });
</script>
<?php 		
 
			unset($_SESSION['c']);
			unset($_SESSION['i']);
			unset($_SESSION['a']);
			unset($_SESSION['t']);
	echo "<script>location.href='../inicioTrabajador.php'</script>";
?>
