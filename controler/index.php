<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		header("Location: inicioTrabajador.php");
	}
	else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/neri.css">
	<title>Inicio</title>
</head>
<body>
	<div id="form1">
		<div id="titulo-form1">
			Logeate
		</div>
		<div id="logo-form1">
			<img src="img/logo.gif" width="100%" alt="">
		</div>
		<form method="POST" action="controler/validar.php">
    		<input type="text" class="ipt-i1" name="use" placeholder="User" autocomplete="off">
    		<input type="password" class="ipt-i1" name="pas" placeholder="Password">
    			<select name="idSucursal" class="ipt-i1" >
				<option value="">Nombre de la sucursal</option>
					<?php 
						require("controler/connect_db.php");
						$consulta2 = "SELECT * FROM sucursales";
						$rs2 = mysqli_query($link,$consulta2);
						while ($row2 = mysqli_fetch_array($rs2)) {
							echo '<option value="'.$row2[0] .'">'.$row2[1] .'</option> ';
						 }
					 ?>
			</select>
    		<input type="submit" class="btn-i1" value="Ingresa">
    	</form>
	</div>
	<footer>
		<div id="img-arka">
			<img src="img/arka2.png" alt="" width="100%"> 
		</div>
			ARKA soluciones
	</footer>
</body>
</html>
<?php 
	}
 ?>