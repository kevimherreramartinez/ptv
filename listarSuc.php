<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos5.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	 <div id="w100lb">
    	<h3>
    		Listado de Sucursales
    	</h3>
    </div>
    <div id="tablaw100">
    	<div id="indicew100">
    		<div class="indiceNumeros">
    			#
    		</div>
    		<div class="indiceNombre">
    			Nombre
    		</div>
    		<div class="indiceNombre">
    			Direccion
    		</div>
    			<div class="indiceTelefonos">
    			Telefonos
    		</div>
    		<div class="indiceClave">
    			Horario Matutino
    		</div>
    		<div class="indiceClave">
    			Horario Vespertino
    		</div>
    		<div class="indiceNumeros">
    			Editar
    		</div>
    	</div>
    	<?php 
    		include("controler/connect_db.php");
    		$pro=mysqli_query($link,"SELECT * FROM sucursales ");
    		while ($prod=mysqli_fetch_array($pro)) {
    			echo '<div id="fila">
		    			<div class="indiceNumeros">
					    			'.$prod[0] .'
					    		</div>
			    		<div class="filaNombre">
			    			'.$prod[1] .'
			    		</div>
			    		<div class="filaNombre">
			    			'.$prod[2] .'
			    		</div>
			    		<div class="filaTelefonos">
			    			'.$prod[3] .' - '.$prod[4] .'
			    		</div>
			    		<div class="filaClave">
			    			'.$prod[6] .' a '.$prod[7] .'
			    		</div>
			    		<div class="filaClave">
			    			'.$prod[8] .' a '.$prod[9] .'
			    		</div>
			    		<div class="filaNumeros">
			    			<a href="editarSucu.php?idSuc='.$prod[0] .'">Editar</a>
			    		</div>
			    		
			    	</div>';
    		}
    	 ?>
    	
    </div>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>