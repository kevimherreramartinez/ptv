<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	 <div id="form1">
    	<div id="titulo-form1">
    		Sucursal nueva
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insSuc.php">
    		<input type="text" name="nom" class="ipt-i1" placeholder="Nombre" autocomplete="off" required>
    		<input type="text" name="dir" class="ipt-i1" placeholder="Direccion" autocomplete="off" required>
  			<input type="text" name="t1" class="ipt-i1" placeholder="Telefono Celular" autocomplete="off" required>
  			<input type="text" name="t2" class="ipt-i1" placeholder="Telefono Fijo" autocomplete="off" required>
  			<div id="form-peque">Turno Matutino</div>
  			<input type="time" name="turno1" class="ipt-i1" placeholder="Hora entrada" autocomplete="off" required>
  			<div id="form-peque">a</div>
  			<input type="time" name="turno2" class="ipt-i1" placeholder="Hora entrada" autocomplete="off" required>
  			<div id="form-peque">Turno Vespertino</div>
  			<input type="time" name="turno3" class="ipt-i1" placeholder="Hora entrada" autocomplete="off" required>
  			<div id="form-peque">a</div>
  			<input type="time" name="turno4" class="ipt-i1" placeholder="Hora entrada" autocomplete="off" required>
  			
    		<input type="submit" class="btn-i1">
    	</form>
    </div>
    <div id="demas">
      <div id="w100lb">
      <h3>
        Listado 
      </h3>
    </div>
    <div id="tablaw100">
      <div id="indicew100">
        <div class="indiceNumeros">
          #
        </div>
        <div class="indiceNombre">
          Nombre
        </div>
        <div class="indiceNombre">
          Direccion
        </div>
          <div class="indiceTelefonos">
          Telefonos
        </div>
        <div class="indiceClave">
           Hor. 1
        </div>
        <div class="indiceClave">
           Hor. 2
        </div>
        <div class="indiceNumeros">
          Editar
        </div>
      </div>
      <?php 
        include("controler/connect_db.php");
        $pro=mysqli_query($link,"SELECT * FROM sucursales ");
        while ($prod=mysqli_fetch_array($pro)) {
          echo '<div class="filaB">
              <div class="indiceNumeros">
                    '.$prod[0] .'
                  </div>
              <div class="filaNombre">
                '.$prod[1] .'
              </div>
              <div class="filaNombre">
                '.$prod[2] .'
              </div>
              <div class="filaTelefonos">
                '.$prod[3] .' - '.$prod[4] .'
              </div>
              <div class="filaClave">
                '.$prod[6] .' a '.$prod[7] .'
              </div>
              <div class="filaClave">
                '.$prod[8] .' a '.$prod[9] .'
              </div>
              <div class="filaNumeros">
                <a href="editarSucu.php?idSuc='.$prod[0] .'">Editar</a>
              </div>
              
            </div>';
        }
       ?>
      
    </div>
    </div>
    <style>
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>