<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	  <style>
		#table{
			width: 90%;
			background: rgba(0,0,0,.1);
			margin: 1em auto;
			padding: 0 2%;
			text-align: center;
		}
		#indice{
			background: rgba(0,0,0,.3);
		}
		.indice,.fila{
			padding: .3em 0%;
			display: inline-block;
			width: 48%;
		}
		.fila{
			font-size: 1.2em;
			border-bottom: 3px solid rgba(0,0,0,.2);
		}
    </style>
    <script type="text/javascript">
		$(function() {
		         
		           $("#nombre").autocomplete({
		                source: "productos6.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
							
							$('#idCliente').val(ui.item.idCliente);
					     }
		            });
				});
</script>
	<title>Inicio</title>
</head>
<body>
		<?php include("menuPrueba.php") ?>
    	<h3>
    		Pago semanal
    	</h3>
    	<div id="table">
    		<div class="indice">Concepto</div>
    		<div class="indice">Total</div>
    		</br>
    		<div class="indice">Pago anterior</div>
    		<div class="indice"><?php echo $nomm[10] ?></div>
    		</br>
    		<div class="indice">Comision anterior</div>
    		<div class="indice"><?php echo $nomm[11] ?></div>
    		</br>
    		<div class="indice">Adelanto anterior</div>
    		<div class="indice"><?php echo $nomm[12] ?></div>
    		</br>
    		<div class="indice">Total anterior</div>
    		<div class="indice"><?php echo $nomm[9] ?></div>
    	</div>
		
	 
</body>

</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>