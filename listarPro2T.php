<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$id_producto=$_POST['id_producto'];
		$idSuc=$_SESSION['suc'];
		$_SESSION['suc']=$idSuc;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4T.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		            $("#nombre").autocomplete({
		                source: "productos5T.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           
				});
</script>
	<title>Inicio</title>
</head>
<body>
<?php include("menuPrueba.php") ?>
    	<div id="w100lb">
    		Busqueda realizada de un producto
    	</div>
	 <div id="form2">
    	<form method="POST" enctype="multipart/form-data" action="listarPro2T.php">
    		<input type="hidden" name="id_producto" id="id_producto">
    		<input type="text" name="clave" id="clave" class="ipt-i2" placeholder="Clave" autocomplete="off" required>
    		<input type="text" name="nom" id="nombre" class="ipt-i2" placeholder="Nombre" autocomplete="off" required>
    		<input type="submit" class="btn-i2" value="Buscar">
    	</form>
    </div>
    <div id="tablaw100">
    	<div id="indicew100">
    		<div class="indiceNumeros">
    			#
    		</div>
    		<div class="indiceNombre">
    			Nombre
    		</div>
    		<div class="indiceClave">
    			Clave
    		</div>
    		<div class="indiceNumeros">
    			Precio 
    		</div>
    		<div class="indiceNumeros">
    			Almacen
    		</div>
    		<div class="indiceLargo">
    			Descripcion
    		</div>	
    		<div class="indiceClave">
    			Img
    		</div>	
    	</div>
    	<?php 
    		include("controler/connect_db.php");
    		$contador=1;
    		$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$idSuc'  AND idProducto='$id_producto'");
    		while ($prod=mysqli_fetch_array($pro)) {
    			echo '<div id="fila" class="filaB" >

		    			<div class="filaNumeros">
					    			'.$contador .'
					    		</div>
			    		<div class="filaNombre">
			    			'.$prod[1] .'
			    		</div>
			    		<div class="filaClave">
			    			'.$prod[6] .'
			    		</div>
			    		<div class="filaNumeros">
			    			$'.$prod[2] .'
			    		</div>
			    		<div class="filaNumeros">
			    			'.$prod[7] .'
			    		</div>
			  			<div class="filaLargo">
			    			'.$prod[8] .'
			    		</div>
			    		<div class="filaClave">';
			    			$nombre_fichero = 'productos2/'.$prod[1].'.jpg';
					    	if (file_exists($nombre_fichero)) {
                                echo '<a href="productos2/'.$prod[1].'.jpg" target="_blank" >Imagen</a>';
                            } else {
                                echo '<a href="productos2/0.png" target="_blank" >Imagen</a>';
                             }
		    				;
					    	echo'
			    		</div>
			    		
			    	</div>';
			    	$contador=$contador+1;
    		}
    	 ?>
    	
    </div>
    <style>
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>