<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$ids=$_GET['idPro'];
		$_SESSION['suc']=1;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">	
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	<link rel="stylesheet" type="text/css" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos05.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		            $("#clave2").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre2').val(ui.item.nombre);
		                    $('#clave2').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre2").autocomplete({
		                source: "productos05.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre2').val(ui.item.nombre);
		                    $('#clave2').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	 <div id="form2">
    	<div id="w100lbt">
    		Listado de productos
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="listarPro2.php">
    		<input type="hidden" value="<?php echo $ids ?>" name="idSucursal">
    		<input type="hidden" name="id_producto" id="id_producto">
    		<input type="text" name="clave" id="clave" class="ipt-i2" placeholder="Clave" autocomplete="off" required>
    		<input type="text" name="nom" id="nombre" class="ipt-i2" placeholder="Nombre" autocomplete="off" required>
    		<input type="submit" class="btn-i2" value="Buscar">
    	</form>
    </div>
   
    <div id="tablaw100">
     <div id="agregar"><a href="#openModal4">Agregar inventario</a></div>
     <div id="agregar"><a href="#openModal2">Nuevo Producto</a></div>
     <div id="agregar"> <a href="listarProDesc.php">Desactivados</a></div>
     <div id="agregar"><a href="#openModal3">Nueva Promocion</a></div>
     <div id="agregar"><a href="listarKit.php?idPro=1">Promociones</a></div>
		<form method="POST" action="controler/agrExi.php">
			<input type="hidden" value="<?php echo $ids ?>" name="idSucursal2">
    	<?php 
    	$sumaSuc=0;
    	$sumaSuc2=0;
    	$sumaTot=0;
    	$sumaInput=1;
    	
    		include("controler/connect_db.php");
    		$prow=mysqli_query($link,"SELECT * FROM sucursales where idSucursal='$ids'  ");
    		while ($prodw=mysqli_fetch_array($prow)) {
    			echo "<div id='w100lbt'> SUCURSAL ".$prodw[0]." , ".$prodw[1]." ".$prodw[2]."</div>";
    			echo      '
					<div id="indicew100">
			    		<div class="indiceNumeros">
			    			#
			    		</div>
			    		<div class="indiceMedioLargo">
			    			Nombre
			    		</div>
			    		<div class="indiceClave">
			    			Clave
			    		</div>
			    		<div class="indiceClave">
			    			Compra
			    		</div>
			    		<div class="indiceClave">
			    			Venta
			    		</div>
			    		
			    		
			    		<div class="indiceNumeros">
			    			Editar
			    		</div>
			    		<div class="indiceNumeros">
			    			Almacen
			    		</div>
			    		<div class="indiceNumeros">
			    			 Min
			    		</div>
			    		<div class="indiceNumeros">
			    			Faltante
			    		</div>
			    		<div class="indiceNumeros">
			    			Excedente
			    		</div>
			    	</div>';
		    				$contador=1;
	    		$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND status=1 order by nombre   ");
		    		while ($prod=mysqli_fetch_array($pro)) {
		    			if ($prod[7]>1 ) {
		    				$cs="filaB";
		    			}
		    			if ($prod[7]==1 ) {
		    				$cs="filaBG";
		    			}
		    			if ($prod[7]<1 ) {
		    				$cs="filaBR";
		    			}
		    			$faltante=$prod[10]-$prod[7];
		    			if ($faltante<0) {
		    				$faltante=0;
		    			}
		    			$sobrante=$prod[7]-$prod[10];
		    			if ($sobrante<0) {
		    				$sobrante=0;
		    			}
		    			echo '
		    			<div id="fila" class="'.$cs.'" >
				    			<div class="indiceNumeros">
							    			'.$contador.'
							    		</div>
					    		<div class="filaMedioLargo">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="filaClave">
					    			'.$prod[6] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[3] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[2] .'
					    		</div>
					    		
					    	
					    		<div class="filaNumeros">
		    						<a href="ediProA.php?idPro='.$prod[0] .'">Editar</a>
					    		</div>
					    	
					    		<div class="indiceNumeros">
								    <input type="hidden" name="'.$sumaInput.'" value="'.$prod[0].'">
								   ';
								   $sumaInput=$sumaInput+1;
								   echo'
								    	<input type="number" value="'.$prod[7] .'" name="'.$sumaInput.'" class="ipt-alm" required autocomplete="off">
								     ';
								   $sumaInput=$sumaInput+1;
								   echo'
								    	<input type="hidden" value="'.$prod[1] .'" name="'.$sumaInput.'" class="ipt-alm" required autocomplete="off">
								     ';
								   $sumaInput=$sumaInput+1;
								   echo'

							    </div>
								<div class="indiceNumeros">
					    			 '.$prod[10].'
					    		</div>
					    		<div class="indiceNumeros">
					    			 '.$faltante.'
					    		</div>
					    		<div class="indiceNumeros">
					    			 '.$sobrante.'
					    		</div>
					    		<div class="filaNumeros">
					    				<a href="controler/dp.php?idPro='.$prod[0] .'">Desactivar</a>
					    		</div>
					    </div>';
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
					    	$sumaSuc2=$sumaSuc2+($prod[7]*$prod[2]);
					    	$ganacia=$sumaSuc2-$sumaSuc;
					    	$contador=$contador+1;
		    		}
		    		 echo'		
		    		 			<input type="hidden" name="idSucursal" class="ipt-i1" value="0" required>
								<input type="submit" value="Realizar Cambios" class="btn-i1">
						    <div id="w100lbt">
						    	<h3>
						    		Total invertido en sucursal : $'.$sumaSuc.'
						    	</h3>
						    	<h3>
						    		Total de Ganancia en sucursal : $'.$ganacia.'
						    	</h3>
						    </div>';
						    $sumaTot=$sumaTot+$sumaSuc;
						    $sumaSuc=0;
						   
						 
					
    				
						  
		    	}
				   
		    	 ?>
								    </form>
    </div>

<div id="openModal2" class="modalDialog2">
  <div >
	<a href="#close2" title="Close2" class="close2">X</a>
    <div id="formModal">
    	<div id="tituloModal">
    		Producto nuevo
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insPro.php">
    		<input type="hidden" name="idSuc" value="<?php echo $idSuc ?>"></br>
    		<input type="text" name="clave" class="ipt-iModal" placeholder="Clave" autocomplete="off"  value="0">
    		<input type="text" name="nom" class="ipt-iModal" placeholder="Nombre" autocomplete="off" required autofocus="">
    		<input type="number" name="ven" step="any" class="ipt-iModal" placeholder="Precio Venta" autocomplete="off" step="0.01" required>
    		<input type="number" name="com" step="any" class="ipt-iModal" placeholder="Precio Compra" autocomplete="off" step="0.01" required>
            <input type="number" name="can" step="any" class="ipt-iModal" placeholder="Cantidad" autocomplete="off" step="0.01" required value="0">
             <input type="number" name="sto" step="any" class="ipt-iModal" placeholder="Stock Minimo" autocomplete="off" step="0.01" required value="0">
            <input type="hidden" name="mis" step="any" class="ipt-iModal" placeholder="Comision" value="0" autocomplete="off" step="0.01" required>
    		<textarea name="des" class="ipt-iModal" cols="30" rows="5" placeholder="Caracteristicas" value="maxima calidad" required>maxima calidad</textarea>
    		<input type="submit" class="btn-iModal" value="Guardar">
    	</form>
    </div>
  </div>
</div>
<div id="openModal3" class="modalDialog3">
  <div >
	<a href="#close3" title="Close2" class="close3">X</a>
    <div id="formModal">
    	<div id="tituloModal">
    		Agregar promocion
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insKit.php">
    		<?php  
    				$dcd=mysqli_query($link,"SELECT count(*) FROM kit");
    				$bfrd=mysqli_fetch_array($dcd);
    			?>
    		<input type="hidden" name="clave" class="ipt-iModal" value="<?php echo $bfrd[0] ?>" autocomplete="off"  >
    		<select name="idPro" id="" class="ipt-iModal" required>
    			<option value="">Selecciona un producto</option>
    			<?php  
    				$ddd=mysqli_query($link,"SELECT * FROM producto where status=1 order by nombre ");
    				while ($dddd=mysqli_fetch_array($ddd)) {
    			?>
    			<option value="<?php echo $dddd[0] ?>"><?php echo $dddd[1] ?></option>4
    		<?php } ?>
    		</select>
    		<input type="text" name="nom" class="ipt-iModal" placeholder="Nombre" autocomplete="off" required autofocus="">
    		<input type="number" name="pro" step="any" class="ipt-iModal" placeholder="Numero Productos" autocomplete="off" step="0.01" required>
    		<input type="number" name="pre" step="any" class="ipt-iModal" placeholder="Precio" autocomplete="off" step="0.01" required>
           
            
    		<input type="submit" class="btn-iModal" value="Guardar">
    	</form>
    </div>
  </div>
</div>
<div id="openModal4" class="modalDialog4">
  <div >
	<a href="#close4" title="Close4" class="close4">X</a>
    <div id="formModal">
    	<div id="tituloModal">
    		Agregar existencia
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insKit.php">
    		<input type="text" name="nom" id="clave2" class="ipt-iModal" placeholder="Clave" autocomplete="off" required autofocus="">
    		<input type="text" name="nom" id="nombre2" class="ipt-iModal" placeholder="Producto" autocomplete="off" required autofocus="">
    		<input type="number" name="pre" step="any" class="ipt-iModal" placeholder="Cantidad" autocomplete="off" step="0.01" required> 
    		<input type="submit" class="btn-iModal" value="Guardar">
    	</form>
    </div>
  </div>
</div>
   
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>