<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
	<title></title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	 <div id="form1">
    	<div id="titulo-form1">
    		Crea una nueva oferta
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insOfer.php">
    		<h3>Fecha de inicio</h3>
    		<input type="date" name="ini" class="ipt-i1" autocomplete="off" required>
    		<h3>Fecha de fin</h3>
    		<input type="date" name="fin" class="ipt-i1" autocomplete="off" required>
			<input type="number" name="pre" step="0.01" class="ipt-i1" placeholder="Precio" autocomplete="off" required>
			<h3>Clave del producto</h3>
			<select name="clave" class="ipt-i1" required>
					<option value="0">Elije</option>
					<?php 
						require("controler/connect_db.php");
						$consulta2 = "SELECT * FROM producto GROUP BY nombre  ";
						$rs2 = mysqli_query($link,$consulta2);
						while ($row2 = mysqli_fetch_array($rs2)) {
							echo '<option value="'.$row2[6] .'">'.$row2[1] .'</option> ';
						 }
					 ?>
			</select>
    		<input type="submit" class="btn-i1">
    	</form>
    </div>
    <style>
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>