<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$id_producto=$_POST['id_producto'];
		$idSucursal=$_POST['idSucursal'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" type="text/css" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos05.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	 <div id="form2">
    	<div id="w100lbt">
    		Listado de productos
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="listarPro2.php">
    		<input type="hidden" value="<?php echo $idSucursal ?>" name="idSucursal">
    		<input type="hidden" name="id_producto" id="id_producto">
    		<input type="text" name="clave" id="clave" class="ipt-i2" placeholder="Clave" autocomplete="off" required>
    		<input type="text" name="nom" id="nombre" class="ipt-i2" placeholder="Nombre" autocomplete="off" required>
    		<input type="submit" class="btn-i2" value="Buscar">
    	</form>
    </div>
    <style>
		#table{
			width: 90%;
			font-size: 15px;
			margin: 1em auto;
			padding: 0 2%;
		}
		#indice{
			background: rgba(0,0,0,.3);
		}
		.indice,.fila{
			padding: .3em 1%;
			display: inline-block;
			width: 21%;
		}

		.indice2,.fila2{
			padding: .3em 1%;
			display: inline-block;
			width: 8%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indice22,.fila22{
			padding: .3em 1%;
			display: inline-block;
			width: 3%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.fila{
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
    </style>
    <div id="tablaw100">
		<form method="POST" action="controler/agrExi.php">
			<input type="hidden" value="<?php echo $idSucursal ?>" name="idSucursal2">
    	<?php 
    	$sumaSuc=0;
    	$sumaSuc2=0;
    	$sumaTot=0;
    	$sumaInput=1;
    	
    		include("controler/connect_db.php");
    		$prow=mysqli_query($link,"SELECT * FROM sucursales where idSucursal='$idSucursal'  ");
    		while ($prodw=mysqli_fetch_array($prow)) {
    			echo "<div id='w100lbt'> SUCURSAL ".$prodw[0]." , ".$prodw[1]." en ".$prodw[2]."</div>";
    			echo      '
					<div id="indicew100">
			    		<div class="indiceNumeros">
			    			#
			    		</div>
			    		<div class="indiceMedioLargo">
			    			Nombre
			    		</div>
			    		<div class="indiceClave">
			    			Clave
			    		</div>
			    		<div class="indiceClave">
			    			Compra
			    		</div>
			    		<div class="indiceClave">
			    			Venta
			    		</div>
			    		
			    		
			    		<div class="indiceNumeros">
			    			Editar
			    		</div>
			    		<div class="indiceNumeros">
			    			Almacen
			    		</div>
			    		<div class="indiceNumeros">
			    			 Min
			    		</div>
			    		<div class="indiceNumeros">
			    			Faltante
			    		</div>
			    		<div class="indiceNumeros">
			    			Excedente
			    		</div>
			    	</div>';
	    		$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND status=1 AND nombre='$id_producto'  ");
	    	//	$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND idProducto='$id_producto' ");
		    				$contador=1;
		    
		    		while ($prod=mysqli_fetch_array($pro)) {
		    			if ($prod[7]>1 ) {
		    				$cs="filaB";
		    			}
		    			if ($prod[7]==1 ) {
		    				$cs="filaBG";
		    			}
		    			if ($prod[7]<1 ) {
		    				$cs="filaBR";
		    			}
		    			$faltante=$prod[10]-$prod[7];
		    			if ($faltante<0) {
		    				$faltante=0;
		    			}
		    			$sobrante=$prod[7]-$prod[10];
		    			if ($sobrante<0) {
		    				$sobrante=0;
		    			}
		    			echo '
		    			<div id="fila" class="'.$cs.'" >
				    			<div class="indiceNumeros">
							    			'.$contador.'
							    		</div>
					    		<div class="filaMedioLargo">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="filaClave">
					    			'.$prod[6] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[3] .'
					    		</div>
					    		<div class="filaClave">
					    			$'.$prod[2] .'
					    		</div>
					    		
					    	
					    		<div class="filaNumeros">
		    						<a href="ediProA.php?idPro='.$prod[0] .'">Editar</a>
					    		</div>
					    	
					    		<div class="indiceNumeros">
								    <input type="hidden" name="'.$sumaInput.'" value="'.$prod[0].'">
								   ';
								   $sumaInput=$sumaInput+1;
								   echo'
								    	<input type="number" value="'.$prod[7] .'" name="'.$sumaInput.'" class="ipt-alm" required autocomplete="off">
								     ';
								   $sumaInput=$sumaInput+1;
								   echo'
								    	<input type="hidden" value="'.$prod[1] .'" name="'.$sumaInput.'" class="ipt-alm" required autocomplete="off">
								     ';
								   $sumaInput=$sumaInput+1;
								   echo'

							    </div>
								<div class="indiceNumeros">
					    			 '.$prod[10].'
					    		</div>
					    		<div class="indiceNumeros">
					    			 '.$faltante.'
					    		</div>
					    		<div class="indiceNumeros">
					    			 '.$sobrante.'
					    		</div>
					    		<div class="filaNumeros">
					    				<a href="controler/dp.php?idPro='.$prod[0] .'">Desactivar</a>
					    		</div>
					    </div>';
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
					    	$sumaSuc2=$sumaSuc2+($prod[7]*$prod[2]);
					    	$ganacia=$sumaSuc2-$sumaSuc;
					    	$contador=$contador+1;
		    		}
		    		 echo'		<input type="hidden" name="idSucursal" class="ipt-i1" value="0" required>
		    		 			
								<input type="submit" value="Realizar Cambios" class="btn-i1">
						   ';
						    $sumaTot=$sumaTot+$sumaSuc;
						    $sumaSuc=0;
						   
					
    				
						  
		    	}
				   
		    	 ?>
								    </form>
    </div>

   
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>