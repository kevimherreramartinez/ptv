<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
<div id="openModal" class="modalDialog">
  <div >
    <div id="form100">
        <a href="#close" title="Close" class="close">X</a>
        <div id="titulo-form">
            Nuevo Trabajador
        </div>
        <form method="POST" enctype="multipart/form-data" action="controler/insTra.php"></br>
            <input type="text" name="nom" class="ipt-i1" placeholder="Nombre completo" autocomplete="off" required>
            <input type="text" name="use" class="ipt-i1" placeholder="Usuario" autocomplete="off" required>
            <input type="password" name="pas" class="ipt-i1" placeholder="Password" autocomplete="off" required>
            <input type="phone" name="tel" class="ipt-i1" placeholder="Telefono" autocomplete="off" required>
            <input type="text" name="dom" class="ipt-i1" placeholder="Direccion" autocomplete="off" required>
            <select name="idSucursal" class="ipt-i1" required>
                    <option value="">Sucursal</option>
                    <?php 
                        require("controler/connect_db.php");
                        $consulta2 = "SELECT * FROM sucursales";
                        $rs2 = mysqli_query($link,$consulta2);
                        while ($row2 = mysqli_fetch_array($rs2)) {
                            echo '<option value="'.$row2[0] .'">'.$row2[0] .' - '.$row2[1] .' - '.$row2[2].'</option> ';
                         }
                     ?>
                </select>
            <h3>Fecha de ingreso</h3>
            <input type="date" name="fec" class="ipt-i1" autocomplete="off" required>
            <input type="submit" class="btn-i1">
        </form>
    </div>
  </div>
</div>
	 <a href="#openModal">
            <div id="modal">
              Agregar Trabajador
            </div>
    </a>
    <div id="w100lbt">

        <h3>
            Listado de Trabajadores activos
        </h3>
        
    </div>
   
    <div id="tablaw100">
    	<div id="indicew100">
            <div class="indiceNumeros">
                #
            </div>
            <div class="indiceNombre">
                Nombre
            </div>
            <div class="indiceTelefono">
                Usuario
            </div>
            <div class="indiceTelefono">
                Password
            </div>
            <div class="indiceNombre">
                Domicilio
            </div>
            <div class="indiceTelefono">
                Telefono
            </div>
            
            <div class="indiceNumeros">
                E
                D
                H
            </div>
            <div class="indiceNumeros">
                Ingreso
            </div>
        </div>
    	<?php 
    		include("controler/connect_db.php");
    		$pro=mysqli_query($link,"SELECT * FROM trabajador where activo=1 ");
    		while ($prod=mysqli_fetch_array($pro)) {
    			$pro2=mysqli_query($link,"SELECT * FROM sucursales where idSucursal='$prod[4] ' ");
    			$prod33=mysqli_fetch_array($pro2);
    			
    			echo '<div class="filaB" >
		    			<div class="filaNumeros">
					    			'.$prod[0] .'
					    		</div>
			    		<div class="filaNombre">
			    			'.$prod[1] .'
			    		</div>
			    		<div class="filaTelefono">
			    			'.$prod[2] .'
			    		</div>
			    		<div class="filaTelefono">
			    			'.$prod[3] .'
			    		</div>
			    		<div class="indiceNombre">
			    			'.$prod[6] .'
			    		</div>
			    		<div class="indiceTelefono">
			    			'.$prod[7] .'
			    		</div>
			    	
			    		<div class="filaNumeros">
			    			<a href="editarTrab.php?idTrab='.$prod[0] .'">X</a>
			    		
			    			<a href="controler/bajaTrab.php?idTrab='.$prod[0] .'">X</a>
			    	
			    			<a href="controler/altaTrab.php?idTrab='.$prod[0] .'">X</a>
			    		</div>
			    	</div>';
    		}
    	 ?>
    
    </div>
       <div id="w100lbt">
    	<h3>
    		Listado de Trabajadores Inactivos
    	</h3>
		
	</div>

    <div id="tw100">
    	<div id="indicew100">
    		<div class="indiceNumeros">
    			#
    		</div>
    		<div class="indiceNombre">
    			Nombre
    		</div>
    		<div class="indiceTelefono">
    			Usuario
    		</div>
    		<div class="indiceTelefono">
    			Password
    		</div>
    		<div class="indiceNombre">
    			Domicilio
    		</div>
    		<div class="indiceTelefono">
    			Telefono
    		</div>
    		
    		<div class="indiceNumeros">
                E
            </div>
                <div class="indiceNumeros">
                H
            </div>
             
    	</div>	
    	<?php 
    		include("controler/connect_db.php");
    		$pro=mysqli_query($link,"SELECT * FROM trabajador where activo=0 ");
    		while ($prod=mysqli_fetch_array($pro)) {
    			$pro2=mysqli_query($link,"SELECT * FROM sucursales where idSucursal='$prod[4] ' ");
    			$prod33=mysqli_fetch_array($pro2);
    			
    			echo '<div class="filaB" >
		    			<div class="filaNumeros">
					    			'.$prod[0] .'
					    		</div>
			    		<div class="filaNombre">
			    			'.$prod[1] .'
			    		</div>
			    		<div class="filaTelefono">
			    			'.$prod[2] .'
			    		</div>
			    		<div class="filaNumeros">
			    			'.$prod[3] .'
			    		</div>
			    		<div class="filaNombre">
			    			'.$prod[6] .'
			    		</div>
			    		<div class="filaTelefono">
			    			'.$prod[7] .'
			    		</div>
			    	
			    		<div class="filaNumeros">
			    			<a href="editarTrab.php?idTrab='.$prod[0] .'">X</a>
			    		</div>
			    		<div class="filaNumeros">
			    			<a href="controler/altaTrab.php?idTrab='.$prod[0] .'">X</a>
			    		</div>
			    		
			    	</div>';
    		}
    	 ?>
    
    </div>
    <style>
		textarea{
			font-family: font;
		}
    </style>
     <style> 
   
   /* aqui empiezan los modales  */
   #titulo-form{
    text-align: center;
    font-weight: bold;
    color: grey;
    font-size: 1.5em;
    margin-top: .5em;
   }
#modal{
    vertical-align: middle;
    width: 14%;
    background: #ED4242;
    text-align: center;
    color: white;
    display: inline-block;
    font-size: 1.1em;
    padding: .2em 0 ; 
    margin: -4em 0 0 5% ;
    border-radius: .2em ;
    box-shadow: 2px 3px 4px rgba(0,0,0,.3);
}

  .modalDialog {
  position: fixed;
  font-family: Arial, Helvetica, sans-serif;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0,0,0,0.8);
  z-index: 99999;
  opacity:0;
  -webkit-transition: opacity 400ms ease-in;
  -moz-transition: opacity 400ms ease-in;
  transition: opacity 400ms ease-in;
  pointer-events: none;
}
.modalDialog:target {
  opacity:1;
  pointer-events: auto;
}
.modalDialog > div {
  width: 400px;
  position: relative;
  margin: 5% auto;
  padding: 5px 20px 13px 20px;
  border-radius: 10px;
  background: white;
  -webkit-transition: opacity 400ms ease-in;
-moz-transition: opacity 400ms ease-in;
transition: opacity 400ms ease-in;
}
.close {
  background: #606061;
  color: #FFFFFF;
  line-height: 25px;
  position: absolute;
  right: -12px;
  text-align: center;
  top: -10px;
  width: 24px;
  text-decoration: none;
  font-weight: bold;
  -webkit-border-radius: 12px;
  -moz-border-radius: 12px;
  border-radius: 12px;
  -moz-box-shadow: 1px 1px 3px #000;
  -webkit-box-shadow: 1px 1px 3px #000;
  box-shadow: 1px 1px 3px #000;
}
.close:hover { background: #00d9ff; }
 </style>
    <style>
        textarea{
            font-family: font;
        }
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>