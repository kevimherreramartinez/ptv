<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
	<title></title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	 <div id="form1">
    	<div id="titulo-form1">
    		Recordatorio
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/recado.php">
    		<h3>Trabajador</h3>
    		<select name="idTrabajador" class="ipt-i1" required>
					<option value="0">Todos</option>
					<?php 
						require("controler/connect_db.php");
						$consulta2 = "SELECT * FROM trabajador where activo=1";
						$rs2 = mysqli_query($link,$consulta2);
						while ($row2 = mysqli_fetch_array($rs2)) {
							echo '<option value="'.$row2[0] .'">'.$row2[1] .'</option> ';
						 }
					 ?>
				</select>
				<h3>fecha programada</h3>
				<input type="date" name="dia" class="ipt-i1" >
    		<input type="text" name="asu" class="ipt-i1" placeholder="Asunto" autocomplete="off" step="0.01" required>
    		<textarea name="msj" class="ipt-i1" cols="30" rows="10" placeholder="Mensaje" required></textarea>
    		<input type="submit" class="btn-i1">
    	</form>
    </div>
    
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>