
<?php 
	require 'fpdf/fpdf.php';
	require("controler/connect_db.php");
	require("controler/funcion.php");
	$dia=date('Y-m-d 00:00:00');
	$dia2=date('Y-m-d 23:59:59');
	$idTra=$_GET['idTra'];
	
	class PDF extends FPDF {
		function Header(){
			require("controler/connect_db.php");
			$dia=date('Y-m-d 00:00:00');
	$dia2=date('Y-m-d 23:59:59');
			$pipi=$dia[8].$dia[9]."-".$dia[5].$dia[6]."-".$dia[0].$dia[1].$dia[2].$dia[3];
			$pipi2=$dia2[8].$dia2[9]."-".$dia2[5].$dia2[6]."-".$dia2[0].$dia2[1].$dia2[2].$dia2[3];
			$this->SetTextColor(49, 49, 49);
			$this->SetFont('Arial' , 'B' , 30);
			$this->Cell(190 , 15 , 'Reporte de Modelorama "Rojo Gomez"' , 0 , 1 , 'C');
			$this->SetTextColor(93, 93, 93);
			$this->SetFont('Arial' , 'B' , 12);
			$this->Cell(190 , 6 , 'Reporte '.$pipi , 0 , 1 , 'C');
		}
	}

	$pdf =new PDF();
	$pdf->AddPage();
	$pdf->SetFont('Arial' , 'B' , 20);
	$pdf->Cell(190 , 20 , 'Ventas Totales' , 0 , 1 , 'C');

	$pdf->SetFont('Arial' , 'B' , 12);

	$pdf->Cell(8 , 10 , 'V' , 0 , 0 , 'L');
	$pdf->Cell(75 , 10 , 'Producto' , 0 , 0 , 'L');
	$pdf->Cell(10 , 10 , 'C' , 0 , 0 , 'L');
	$pdf->Cell(15 , 10 , 'Total' , 0 , 0 , 'L');
	$pdf->Cell(30 , 10 , 'Fecha' , 0 , 1 , 'L');
	$pdf->SetFont('Arial' , '' , 9);

	$venta=1;
	$pedido=1;
	$dinerito=0;
	$ganacia=0;
	$cliente=0;
	$cantPro=0;
	$pipi=$dia.' 00:00:00';
	$pipi2=$dia2.' 23:59:59';
	//$pipi=$dia[0].$dia[1].$dia[2].$dia[3]."-".$dia[5].$dia[6]."-".$dia[8].$dia[9].' 00:00:00';
	//$pipi2=$dia[0].$dia[1].$dia[2].$dia[3]."-".$dia[5].$dia[6]."-".$dia[8].$dia[9].' 23:59:59';
	$result = mysqli_query($link,"SELECT * FROM venta where cancelar=1 and idTrabajador='$idTra' AND  fecha BETWEEN '$pipi' AND '$pipi2' ");
	while ($row = mysqli_fetch_row($result)) { 
		$nom=mysqli_query($link , "SELECT * FROM trabajador where idTrabajador='$row[6] '");
			$nomm=mysqli_fetch_array($nom);
			$suv=mysqli_query($link , "SELECT * FROM sucursales where idSucursal='$row[5] '");
			$suvvv=mysqli_fetch_array($suv);
				$result2 = mysqli_query($link,"SELECT * FROM pedido where idVenta='$row[0] ' ");
						$pdf->SetFillColor(255,255,255);
				
					$pdf->Cell(8 , 5 , $venta , 0 , 0 , 'L' ,True);
					$ladito=0;
					while ($row2 = mysqli_fetch_row($result2)) {
								if ($row2[5]==1) {
									$result3 = mysqli_query($link,"SELECT * FROM producto where idProducto='$row2[2] ' ");
									$row3 = mysqli_fetch_row($result3);
									if ($ladito==1) {
								 		$pdf->Cell(8 , 5 , '' , 0 , 0 , 'L' ,True);	
									}
									$pdf->Cell(75 , 5 , utf8_decode($row3[1])  , 0 , 0 , 'L' ,True);
									$pdf->Cell(10 , 5 , $row2[3] , 0 , 0 , 'L' ,True);
									$pdf->Cell(15 , 5 , '$'.$row2[4] , 0 , 0 , 'L' ,True);
									$pdf->Cell(30 , 5 , $row[1] , 0 , 1 , 'L' ,True);
									
									$dinerito=$row2[4]+$dinerito;
									$ganacia=$ganacia+($row3[3]*$row2[3]);
									$ladito=1;
								 	$pedido=$pedido+1;
									$cantPro=$cantPro+$row2[3];
								}// fin de compra de producto
								if ($row2[5]==2) {
									$result3 = mysqli_query($link,"SELECT * FROM kit where idKit='$row2[2] ' ");
									$row3 = mysqli_fetch_row($result3);

									if ($ladito==1) {
								 		$pdf->Cell(8 , 5 , '' , 0 , 0 , 'L' ,True);	
									}
									$pdf->Cell(75 , 5 , utf8_decode($row3[1])  , 0 , 0 , 'L' ,True);
									$pdf->Cell(10 , 5 , $row2[3] , 0 , 0 , 'L' ,True);
									$pdf->Cell(15 , 5 , '$'.$row2[4] , 0 , 0 , 'L' ,True);
									$pdf->Cell(30 , 5 , $row[1] , 0 , 1 , 'L' ,True);
									
									$dinerito=$row2[4]+$dinerito;
									$ganacia=$ganacia+($row3[3]*$row2[3]);
									$ladito=1;
								 	$pedido=$pedido+1;
									$cantPro=$cantPro+$row2[3];
								}
					}
			$venta=$venta+1;
			$cliente=$cliente+1;

	
	}
	$margen=$dinerito-$ganacia;

	$pdf->SetFont('Arial' , 'B' , 20);
	$pdf->Cell(190 , 20 , 'Salidas Totales' , 0 , 1 , 'C');

	$pdf->SetFont('Arial' , 'B' , 12);

	$pdf->Cell(8 , 10 , 'V' , 0 , 0 , 'L');
	$pdf->Cell(75 , 10 , 'Concepto' , 0 , 0 , 'L');
	$pdf->Cell(10 , 10 , 'C' , 0 , 0 , 'L');
	$pdf->Cell(15 , 10 , 'Total' , 0 , 0 , 'L');
	$pdf->Cell(30 , 10 , 'Fecha' , 0 , 1 , 'L');
	$pdf->SetFont('Arial' , '' , 9);

	$c3=1;
	$tc=0;

	//$pipi=$dia[0].$dia[1].$dia[2].$dia[3]."-".$dia[5].$dia[6]."-".$dia[8].$dia[9].' 00:00:00';
	//$pipi2=$dia[0].$dia[1].$dia[2].$dia[3]."-".$dia[5].$dia[6]."-".$dia[8].$dia[9].' 23:59:59';
	$result33 = mysqli_query($link,"SELECT * FROM salida where activo=1 and fecha BETWEEN '$pipi' AND '$pipi2' ");
	while ($rowde = mysqli_fetch_row($result33)) { 
					$pdf->Cell(8 , 5 , $c3 , 0 , 0 , 'L' ,True);
					$pdf->Cell(75 , 5 , utf8_decode($rowde[2])  , 0 , 0 , 'L' ,True);
					$pdf->Cell(10 , 5 , 1 , 0 , 0 , 'L' ,True);
					$pdf->Cell(15 , 5 , '$'.$rowde[1] , 0 , 0 , 'L' ,True);
					$pdf->Cell(30 , 5 , $rowde[4] , 0 , 1 , 'L' ,True);
			$tc=$tc+$rowde[1];
			$c3=$c3+1;	
	}
	$margen=$dinerito-$ganacia;
	$dfg=$dinerito-$tc;




	$pdf->SetFont('Arial' , 'B' , 12);
	$pdf->Cell(40 , 10 , '' , 0 , 1 , 'L');
	$pdf->Cell(100 , 10 , ' Efectivo obtenido : ' , 0 , 0 , 'R');
	$pdf->Cell(30 , 10 , '$ '.$dinerito , 0 , 1 , 'R');

	

	$pdf->Cell(100 , 10 , ' Salidas de efectivo : ' , 0 , 0 , 'R');
	$pdf->Cell(30 , 10 , '$ '.$tc , 0 , 1 , 'R');

	$pdf->Cell(100 , 10 , ' Efectivo entregado : ' , 0 , 0 , 'R');
	$pdf->Cell(30 , 10 , '$ '.$dfg , 0 , 1 , 'R');

	
	$pdf->Cell(100 , 10 , 'Clientes : ' , 0 ,0 , 'R');
	$pdf->Cell(30 , 10 , $cliente , 0 , 1 , 'R');

	$pdf->Cell(100 , 10 , 'Productos Vendidos : ', 0 , 0 , 'R');
	$pdf->Cell(30 , 10 , $cantPro , 0 , 1 , 'R');



	$pdf->Output();
 	