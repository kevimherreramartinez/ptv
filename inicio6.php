<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#codigo").autocomplete({
		                source: "productos.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#codigo').val(ui.item.codigo);
							$('#descripcion').val(ui.item.descripcion);
							$('#precio').val(ui.item.precio);
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	<title>Inicio</title>
</head>
<body>
	<?php include("menuAdmin.php") ?>
	<div id="tabla">
		
	<div id="tabla-ind">
		<div class="tabla-ind">Nombre</div>
		<div class="tabla-ind">Precio</div>
		<div class="tabla-ind">Cantidad</div>
		<div class="tabla-ind">Total</div>
	</div>
	<script language="javascript"  type="text/javascript">
				function Sumar(){
	 				a=document.f1.precio.value;
	 				b=document.f1.cantidad.value;
	 				c=parseInt(a)*(b);
	 				document.f1.total.value=c;
				}
	</script>
	<form method="POST" action="inicio61.php" name="f1">
		<div class="ui-widget">
		  <input type="hidden" id="id_producto" name="id_producto">
		  <input id="codigo" autofocus class="ipt-punto" name="codigo" >
		  <input id="precio" readonly OnKeyUP="Sumar()"   name="precio" class="ipt-punto">
		  <input type="text" id="cantidad" OnKeyUP="Sumar()" name="cantidad" class="ipt-punto" >
		  <input type="text" id="total"  name="total" class="ipt-punto" name="total">
		  <input type="submit" class="btn-punto">
		</div>
	</form>
	</div>


</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>