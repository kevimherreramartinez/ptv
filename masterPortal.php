<?php 
	session_start();
	ob_start();

	$adivina=1;
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
			$numeroLista=0;
			if (isset($_POST['codigo']) AND isset($_POST['precio']) AND isset($_POST['cantidad']) AND isset($_POST['total'])){
				$codigo=$_POST['codigo'];
				$id_producto=$_POST['id_producto'];
				$clave=$_POST['clave'];
				$precio=$_POST['precio'];
				$cantidad=$_POST['cantidad'];
				$total=$_POST['total'];
			}
			else{
				$adivina=0;
			}
			if (isset($_SESSION['c']) AND isset($_SESSION['p']) AND isset($_SESSION['a']) AND isset($_SESSION['t']) AND isset($_SESSION['l'])){
					$c=$_SESSION['c'];
					$numeroLista=count($_SESSION['c']);
					$l=$_SESSION['l'];
					$i=$_SESSION['i'];
					$p=$_SESSION['p'];
					$a=$_SESSION['a'];
					$t=$_SESSION['t'];
				if (isset($_POST['codigo']) AND isset($_POST['id_producto']) AND isset($_POST['precio']) AND isset($_POST['cantidad'])){
						$c[$numeroLista+1]=$codigo;
						$l[$numeroLista+1]=$clave;
						$i[$numeroLista+1]=$id_producto;
						$p[$numeroLista+1]=$precio;
						$a[$numeroLista+1]=$cantidad;
						$t[$numeroLista+1]=$total;
				}
					$_SESSION['c']=$c;
					$_SESSION['l']=$l;
					$_SESSION['i']=$i;
					$_SESSION['p']=$p;
					$_SESSION['a']=$a;
					$_SESSION['t']=$t;
					$numeroLista=count($_SESSION['c']);
			}
			else{
				if($adivina==1){
							$c[1]=$codigo;
							$i[1]=$id_producto;
							$p[1]=$precio;
							$l[1]=$clave;
							$a[1]=$cantidad;
							$t[1]=$total;
							$_SESSION['c']=$c;
							$numeroLista=count($_SESSION['c']);
							$_SESSION['i']=$i;
							$_SESSION['l']=$l;
							$_SESSION['p']=$p;
							$_SESSION['a']=$a;
							$_SESSION['t']=$t;
				}
			}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/neri.css">
	<title>Inicio</title>
</head>
<body>
	<?php include("menuPruebaAdmin.php") ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function () {
		            $("#codigo").autocomplete({
		                source: "productosC.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#codigo').val(ui.item.codigo);
		                    $('#clave').val(ui.item.clave);
							$('#descripcion').val(ui.item.descripcion);
							$('#precio').val(ui.item.precio);
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		             $("#clave").autocomplete({
		                source: "productos2C.php",
		                minLength: 0,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#clave').val(ui.item.clave);
		                    $('#codigo').val(ui.item.codigo);
							$('#descripcion').val(ui.item.descripcion);
							$('#precio').val(ui.item.precio);
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	<div id="w100lb">Productos para uso personal</div>
	<div id="tabla">
		<?php include("indiceTablaVenta.php") ?>
	
	<script language="javascript"  type="text/javascript">
				function Sumar(){
	 				a=document.f1.precio.value;
	 				b=document.f1.cantidad.value;
	 				c=parseFloat(a)*(b);
	 				document.f1.total.value=c;
				}
			
				   
	</script>
	<style>
			input[data-readonly] {
			  pointer-events: none;
			}
	</style>
	<form method="POST" action="masterPortal.php" name="f1">
		<div class="ui-widget">
			  <input type="hidden" id="id_producto" name="id_producto" required>
			  <input id="clave" autofocus class="ipt-puntoClave" name="clave" required autocomplete="off">
			  <input id="codigo" autofocus class="ipt-puntoNombre" name="codigo" required autocomplete="off">
			  <input type="number" min="0"  step="0.01" id="precio"  required data-readonly  OnKeyUP="Sumar()"   name="precio" class="ipt-puntoPrecio" required autocomplete="off">
			  <input type="number" id="cantidad" OnKeyUP="Sumar()" name="cantidad" min="1" class="ipt-puntoCantidad" autocomplete="off" required >
			  <input type="text" id="total"  readonly name="total" class="ipt-puntoTotal" name="total" c  required autocomplete="off">
			  <input type="submit" class="btn-puntoVenta" value="+">
			</div>
	</form>
		<script language="javascript"  type="text/javascript">
				function Sumartotal(){
	 				aa=document.f2.to.value;
	 				ab=document.f2.ba.value;
	 				ac=parseInt(ab)-(aa);
	 				document.f2.fi.value=ac;
				}
		</script>
	<form method="POST" action="controler/ventaSalidac.php"  onsubmit="return checkSubmit();" name="f2">
	<div id="table2">
		<div id="tabla-ind">
					<div class='tablalarga'>
							<?php 
								$totalPago=0;
								for ($iii=$numeroLista; $iii >=1 ; $iii--) { 
									echo "
										<div class='borderBottom'>
										<div class='tabla-indClave'>".$l[$iii] ."</div>
										<div class='tabla-indNombre2'> ".$c[$iii] ."</div>
										<div class='tabla-indPrecio'>".$p[$iii] ."</div>
										<div class='tabla-indCantidad'>".$a[$iii] ."</div>
										<div class='tabla-indTotal'>".$t[$iii] ."</div>
										<div class='tabla-indCancelar'>  <a href='cancelarProductoC.php?lugar=".$iii."'>X</a></div>
										</div>
									";
									$totalPago=$totalPago+$t[$iii];
								}
							 ?>
						</div>
					</div>
		</div>
				<select name="idSucursal" class="iptCliente" required>
					<option value="">Nombre de la sucursal</option>
					<?php 
						require("controler/connect_db.php");
						$consulta2 = "SELECT * FROM sucursales";
						$rs2 = mysqli_query($link,$consulta2);
						while ($row2 = mysqli_fetch_array($rs2)) {
							echo '<option value="'.$row2[0] .'">'.$row2[1] .'</option> ';
						 }
					 ?>
				</select>
				<div class="btnCancelar">
					<a href="cancelarVentaC.php">
						Cancelar
					</a>
				</div>
				
			<div id="montos">
				<div id="total">Total : <input type="number" min="1"  step="any" readonly class="iptTotal" id="to" name="to" OnKeyUP="Sumartotal()" value="<?php echo $totalPago ?>" required></div>
			
			</div>
	</div>
				<input type="submit" class="btnTotal" value="Cargar">
		</form>
	</div>
<script>
	var statSend = false;
function checkSubmit() {
    if (!statSend) {
        statSend = true;
        return true;
    } else {
        alert("El formulario ya se esta enviando...");
        return false;
    }
}
	
</script>

</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>