<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos5.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	

    <div id="tablaw100">
    	
    	<?php 
    	$sumaSuc=0;
    	$sumaTot=0;
    		include("controler/connect_db.php");
    		$prow=mysqli_query($link,"SELECT * FROM sucursales  ");
    		while ($prodw=mysqli_fetch_array($prow)) {
    			echo "<div id='w100lbt'><h3> SUCURSAL ".$prodw[0]." - ".$prodw[1]." - ".$prodw[2]."</h3></div>";
    			echo      '
					<div id="indice">
			    		<div class="indiceNumeros">
			    			#
			    		</div>
			    		<div class="indiceNombre">
			    			Nombre
			    		</div>
			    		<div class="indiceClave">
			    			Clave
			    		</div>
			    		<div class="indiceNumeros">
			    			Compra
			    		</div>
			    		<div class="indiceNumeros">
			    			Venta
			    		</div>
			    		<div class="indiceNumeros">
			    			Almacen
			    		</div>
			    		<div class="indiceNumeros">
			    			Comision
			    		</div>
			    		<div class="indiceNumeros">
			    			Imagen
			    		</div>
			    		<div class="indiceNumeros">
			    			Editar
			    		</div>
			    	
			    	</div>';
	    		$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' and almacen<=0 ");
	    	//	$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND idProducto='$id_producto' ");
		    		while ($prod=mysqli_fetch_array($pro)) {
		    			
		    			echo '
		    			<div class="filaB"  >
				    			<div class="filaNumeros">
							    			'.$prod[0] .'
							    		</div>
					    		<div class="filaNombre">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="filaClave">
					    			'.$prod[6] .'
					    		</div>
					    		<div class="filaNumeros">
					    			$'.$prod[3] .'
					    		</div>
					    		<div class="filaNumeros">
					    			$'.$prod[2] .'
					    		</div>
					    		<div class="filaNumeros">
					    			'.$prod[7] .'
					    		</div>
					    		<div class="filaNumeros">
					    			'.$prod[9] .'
					    		</div>
					    			<div class="filaNumeros">';
					    		$nombre_fichero = 'productos2/'.$prod[1].'.jpg';
					    		if (file_exists($nombre_fichero)) {
                                    echo '<a href="productos2/'.$prod[1].'.jpg" target="_blank" >Imagen</a>';
                                } else {
                                    echo '<a href="productos2/0.png" target="_blank" >Imagen</a>';
                                }
		    						
					    		echo' </div>
					    		<div class="filaNumeros">
		    						<a href="ediProA.php?idPro='.$prod[0] .'">Editar</a>
					    		</div>
					    	
					    </div>';
					    		/*<div class="filaNombre">
								    <form method="POST" action="controler/agrExi.php">
								    <input type="hidden" name="idPro" value="'.$prod[0].'">
								     <input type="hidden" name="idSuc" value="'.$prod[5].'">
								    	<input type="number" name="alm" class="ipt-alm" required autocomplete="off">
								    	<input type="submit" value="+" class="btn-i2">
								    </form>
							    </div>*/
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
		    		}
		    		 echo'
						    <div id="invert">
						    	Total invertido en sucursal : $'.$sumaSuc.'
						    </div>';
						    $sumaTot=$sumaTot+$sumaSuc;
						    $sumaSuc=0;

					
    				
						  
		    	}
				    echo'
								    <div id="invert2">
								    	Total invertido en todas las sucursales: $'.$sumaTot.'
								    </div>';
		    	 ?>
    </div>

    
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>