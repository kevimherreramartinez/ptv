<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$idSuc=$_SESSION['suc'];
		$_SESSION['suc']=$idSuc;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<title></title>
</head>
<body>
<?php include("menuTrabajador.php") ?>
<?php include("subMenuadminT.php") ?>	
	 <div id="form2">
    	<h3>
    		Producto nuevo
    	</h3>
    	<form method="POST" enctype="multipart/form-data" action="controler/insPro2.php">
    		<input type="hidden" name="idSuc" value="<?php echo $idSuc ?>">
    		<input type="text" name="clave" class="ipt-i" placeholder="Clave" autocomplete="off" required>
    		<input type="text" name="nom" class="ipt-i" placeholder="Nombre" autocomplete="off" required>
    		<input type="number" name="ven" class="ipt-i" placeholder="Precio Venta" autocomplete="off" step="0.01" required>
    		<input type="number" name="com" class="ipt-i" placeholder="Precio Compra" autocomplete="off" step="0.01" required>
    		<input type="number" name="alm" class="ipt-i" placeholder="Almacen" autocomplete="off" required>
    		<input type="submit" class="btn-i">
    	</form>
    </div>
    <style>
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>