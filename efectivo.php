<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" type="text/css" href="css/neri.css">
	<title></title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	<div id="w100lbt">
    	<h3>
    		Efectivo en sucursales
    	</h3>
	</div>
    	<?php 
    		include("controler/connect_db.php");
    		$pro=mysqli_query($link,"SELECT * FROM sucursales  ");
    		while ($prod=mysqli_fetch_array($pro)) {
    			echo '
			    	<div id="su">
			    		<div class="title-suc">
			    			sucursal '.$prod[0] .' </br> '.$prod[1] .' 
			    		</div>
			    		<div class="img-suc">
			    			<img src="img/sucursales.png" width="100%" alt="">
			    		</div>
			    		<div class="mon-suc">
			    			$'.$prod[5] .' 
			    		</div>
			    		<div class="cortes-suc">
			    			<form method="POST" enctype="multipart/form-data" action="controler/modMon.php">
			    				<input type="hidden" name="idSuc" value='.$prod[0] .'>
					    		<input type="text" name="mon" class="ipt-i" placeholder="Nuevo monto" autocomplete="off" required>
					    		
					    		<input type="submit" class="btn-i">
					    	</form>
			    		</div>
				    	 <div class="btn-ii">
				    	 <a href="vercortes.php?idSuc='.$prod[0] .'">Ver cortes</a>
				    	 	
				    	 </div>
			    	</div>';
    		}
    	 ?>
	
    <style>
    
    .btn-ii a{
    	color: white;
    }
		textarea{
			font-family: font;
		}
		#su{
			width: 18%;
			vertical-align: top;
			padding: 0 1%;
			margin: 1em 2%;
			display: inline-block;
			text-align: center;
			border-radius: .2em;
			background: white;
		}
		.title-suc{
			font-size: 1.3em;
			padding: .3em 0;
			color: grey;
			background: rgba(0,0,0,.05);

		}
		.img-suc{
			width: 60%;
			margin: 1em 20%;
		}
		.mon-suc{
			font-size: 1.4em;
			font-weight: bold;
			padding-bottom: .5em;
			color: rgba(0,0,0,.8);
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>