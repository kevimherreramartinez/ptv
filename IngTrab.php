<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
	 <div id="form1">
    	<div id="titulo-form1">
    		Nuevo Trabajador
    	</div>
    	<form method="POST" enctype="multipart/form-data" action="controler/insTra.php"></br>
    		<input type="text" name="nom" class="ipt-i1" placeholder="Nombre completo" autocomplete="off" required>
    		<input type="text" name="use" class="ipt-i1" placeholder="Usuario" autocomplete="off" required>
    		<input type="password" name="pas" class="ipt-i1" placeholder="Password" autocomplete="off" required>
    		<input type="phone" name="tel" class="ipt-i1" placeholder="Telefono" autocomplete="off" required>
    		<input type="text" name="dom" class="ipt-i1" placeholder="Direccion" autocomplete="off" required>
    		<select name="idSucursal" class="ipt-i1" required>
					<option value="">Sucursal</option>
					<?php 
						require("controler/connect_db.php");
						$consulta2 = "SELECT * FROM sucursales";
						$rs2 = mysqli_query($link,$consulta2);
						while ($row2 = mysqli_fetch_array($rs2)) {
							echo '<option value="'.$row2[0] .'">'.$row2[0] .' - '.$row2[1] .' - '.$row2[2].'</option> ';
						 }
					 ?>
				</select>
    		<input type="submit" class="btn-i1">
    	</form>
    </div>
    
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>