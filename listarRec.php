<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
    <style>
		#table{
			width: 90%;
			font-size: 15px;
			margin: 1em auto;
			padding: 0 2%;
		}
		#indice{
			background: rgba(0,0,0,.3);
		}
		.indice,.fila{
			padding: .3em 1%;
			display: inline-block;
			width: 21%;
		}

		.indice2,.fila2{
			padding: .3em 1%;
			display: inline-block;
			width: 8%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indiceM,.filaM{
			padding: .3em 1%;
			display: inline-block;
			width: 30%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indiceF,.filaF{
			padding: .3em 1%;
			display: inline-block;
			width: 14%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indice22,.fila22{
			padding: .3em 1%;
			display: inline-block;
			width: 2%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.fila{
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
    </style>	
    		<a href="controler/eliMsjMasivo.php">
			    	<div class="btn i1">
			    		Eliminar Mensajes Leidos
			    	</div>
    		</a>
				<div id="indicew100">
			    		<div class="filaNombre">
			    			Trabajador
			    		</div>
			    		<div class="filaTelefono">
			    			Asunto
			    		</div>
			    		<div class="filaMedioLargo">
			    			Mensaje
			    		</div>
			    		<div class="indiceNumeros">
			    			Visto
			    		</div>
			    		<div class="indiceBoton">
			    			Fecha
			    		</div>
			    		<div class="indiceNumeros">
			    			Eliminar
			    		</div>
			    		
			    	</div>
    <div id="tablaw100">
    	<?php 
    	$sumaSuc=0;
    	$sumaTot=0;
    		include("controler/connect_db.php");
    		$prow=mysqli_query($link,"SELECT * FROM recordatorio  ");
    		while ($prodw=mysqli_fetch_array($prow)) {
    			$tr=mysqli_query($link,"SELECT * FROM trabajador where idTrabajador='$prodw[1] '  ");
				$tra=mysqli_fetch_array($tr);
		    			if ($prodw[4]==0) {
		    				$res="No";
		    			}
		    			else{
		    				$res="Si";
		    			}
		    			echo '
		    			<div class="filaB"  >
				    			<div class="filaNombre">
					    			'.$tra[1] .'
					    		</div>
					    		<div class="filaTelefono">
					    			'.$prodw[2] .'
					    		</div>
					    		<div class="filaMedioLargo">
					    			'.$prodw[3] .'
					    		</div>
					    		<div class="filaNumeros">
					    			'.$res.'
					    		</div>
					    		<div class="filaBoton">
					    			'.$prodw[5] .'
					    		</div>
					    		
					    		<div class="filaNumeros">
					    			<a href="controler/eliMsj.php?idMsj='.$prodw[0] .'">x</a>
					    		</div>
					    		
					    	</div>';
		    		
		    		
		    	}
				    
		    	 ?>
    </div>
    <style>
	    #invert{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	margin-left: 5%;
	    	font-size: 1.2em;
	    }
	      #invert2{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	
	    	font-size: 1.4em;
	    }
		.ipt-alm{
			width: 60%;
			display: inline-block;
			border: none;
			border-radius: .2em;
		}
		.btn-alm{
			border: none;
			background: grey;
			color: white;
			width: 30%;
			display: inline-block;
		}
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>