<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
		$idSuc=$_SESSION['suc'];
		$_SESSION['suc']=$idSuc;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<title>Inicio</title>
</head>
<body>
<?php include("menuAdmin.php") ?>
<?php include("subMenuadminGarantia.php") ?>
    	<div id="form2">
    	<h3>
    		Garantia
    	</h3>
    	<form method="POST" enctype="multipart/form-data" action="controler/insGarA.php">
    		<input type="hidden" name="idSuc" value="<?php echo $idSuc ?>">
    		<input type="hidden" name="idTra" value="<?php echo $nomm[0] ?>">
    		<input type="text" name="cla" class="ipt-i" placeholder="Clave del producto" autocomplete="off" required>
    		<input type="text" name="nom" class="ipt-i" placeholder="Nombre del producto" autocomplete="off" required>
    		<textarea name="con"  placeholder="Falla" class="ipt-i" cols="30" rows="10" required></textarea>
            <input type="text" name="int" class="ipt-i" placeholder="Nombre del interesado" autocomplete="off" required>
            <input type="text" name="tel" class="ipt-i" placeholder="Telefono" autocomplete="off" required>
            <input type="text" name="dir" class="ipt-i" placeholder="Direccion" autocomplete="off" required>
    		
    		<input type="submit" class="btn-i">
    	</form>
  		
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>