<?php 
	session_start();
	ob_start();
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/neri.css">
	<title>Inicio</title>
</head>
<body>
	<div id="formLogeo">
		<div id="logo-formLogeo">
			<img src="img/kajo.png" width="100%" alt="">
		</div>
		
		<form method="POST" action="controler/validar.php">
    		<input type="text" class="ipt-iLogeo" id="u" name="use" placeholder="User" autocomplete="off">
    		<input type="password" class="ipt-iLogeo" id="c" name="pas" placeholder="Password">
    		
    		<input type="submit" class="btn-iLogeo" value="Siguiente >>">
    	</form>
	</div>
	<footer>
		<div id="img-arka">
			<img src="img/arka2.png" alt="" width="100%"> 
		</div>
			ARKA SOLUCIONES
	</footer>
</body>
</html>
