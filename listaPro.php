<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
   <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos5.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	<title>Inicio</title>
</head>
<body>
<?php include("menuAdmin.php") ?>
    	<h3>
    		Pedido a Proveedor
    	</h3>
    	
    <style>
		#table{
			width: 90%;
			font-size: 18px;
			margin: 1em auto;
			padding: 0 2%;
		}
		#indice{
			background: rgba(0,0,0,.3);
		}
		.indice,.fila{
			padding: .3em 1%;
			display: inline-block;
			width: 40%;
		}

		.indice2,.fila2{
			padding: .3em 1%;
			display: inline-block;
			width: 11%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indiceD,.filaD{
			margin-bottom: 1em;
			background: rgba(0,0,0,.2);
			padding: .3em 1%;
			display: inline-block;
			width: 98%;
			font-size: 1.2em;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.indice22,.fila22{
			padding: .3em 1%;
			display: inline-block;
			width: 6%;
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
		.fila{
			border-bottom: 1px solid rgba(0,0,0,.2);
		}
    </style>
    <div id="table">
    	
    	<?php 
    	$sumaSuc=0;
    	$sumaTot=0;
    		include("controler/connect_db.php");
    	
    			echo      '
					<div id="indice">
			    		<div class="indice">
			    			Nombre
			    		</div>
			    		<div class="indice2">
			    			Clave
			    		</div>
			    	
			    		<div class="indice2">
			    			Pedido
			    		</div>
			    	</div>';
	    		$pro=mysqli_query($link,"SELECT * FROM producto where status=1 group by clave ");
	    	//	$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND idProducto='$id_producto' ");
		    		while ($prod=mysqli_fetch_array($pro)) {
		    				if ($prod[4]==0 ) {
		    				$csss="filaC";
		    				echo "	<style>
									.filaC{
										background: #DA244C;
										color:white;
									}
									.filaC a{
											color:white;
									}
						    	</style>";
		    				}
		    			else{
		    				$csss="filaB";
		    				echo "
									<style>
										.filaB{
											background:#2F74D5;
											color:white;
										}
										.filaB a{
											color:white;
										}
							    	</style>
		    				"	;
		    			}
		    			echo '
		    			<div id="fila" class="'.$csss.'" >
				    			
					    		<div class="fila">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="fila2">
					    			'.$prod[6] .'
					    		</div>
								<div class="fila22">';
					    		
					    		echo' </div>
					    		<div class="fila22">
					    			<input type="text" value="0" name="'.$prod[0] .'"  >
					    		</div>
					    	</div>';
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
		    		}
		    		
				   
		    	 ?>
    </div>
    <style>
	    #invert{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	margin-left: 5%;
	    	font-size: 1.2em;
	    }
	      #invert2{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	
	    	font-size: 1.4em;
	    }
		.ipt-alm{
			width: 60%;
			display: inline-block;
			border: none;
			border-radius: .2em;
		}
		.btn-alm{
			border: none;
			background: grey;
			color: white;
			width: 30%;
			display: inline-block;
		}
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>