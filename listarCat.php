<?php 
	session_start();
	ob_start();
	if (isset($_SESSION['use']) AND isset($_SESSION['pas'])){
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/style2.css">
	<link rel="stylesheet" href="css/neri.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<title>Inicio</title>
</head>
<body>
<?php include("menuPruebaAdmin.php") ?>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript">
		$(function() {
		            $("#clave").autocomplete({
		                source: "productos4.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
		           $("#nombre").autocomplete({
		                source: "productos5.php",
		                minLength: 1,
		                select: function(event, ui) {
							event.preventDefault();
		                    $('#nombre').val(ui.item.nombre);
		                    $('#clave').val(ui.item.clave);
							
							$('#id_producto').val(ui.item.id_producto);
					     }
		            });
				});
</script>
	 <div id="w100lbt">
	 	<h3>
    		Catalogo de productos
	 	</h3>
    </div>
 	 <div id="form2">
    	
    	<form method="POST" enctype="multipart/form-data" action="listarCat22.php">
    		<input type="hidden" value="<?php echo $ids ?>" name="idSucursal">
    		<input type="hidden" name="id_producto" id="id_producto">
    		<input type="text" name="clave" id="clave" class="ipt-i2" placeholder="Clave" autocomplete="off" required>
    		<input type="text" name="nom" id="nombre" class="ipt-i2" placeholder="Nombre" autocomplete="off" required>
    		<input type="submit" class="btn-i2" value="Buscar">
    	</form>
    </div>
    <div id="tablaw100">
    	<?php 
    	$sumaSuc=0;
    	$sumaTot=0;
    		include("controler/connect_db.php");
    			echo      '
					<div id="indicew100">
			    		
			    		<div class="indiceNombre">
			    			Nombre
			    		</div>
			    		<div class="indiceClave">
			    			Clave
			    		</div>
			    		<div class="indiceNumeros">
			    			Compra
			    		</div>
			    		<div class="indiceNumeros">
			    			Venta
			    		</div>
			    		
			    		<div class="indiceTelefono">
			    			Desactivar
			    		</div>
			    		<div class="indiceNumeros">
			    			Activar
			    		</div>
			    		<div class="indiceNumeros">
			    			Imagen
			    		</div>
			    		<div class="indiceNumeros">
			    			Activo
			    		</div>
		
			    	</div>';
	    		$pro=mysqli_query($link,"SELECT * FROM producto where status=1 group by nombre ");
	    	//	$pro=mysqli_query($link,"SELECT * FROM producto where idSucursal='$prodw[0] ' AND idProducto='$id_producto' ");
		    		while ($prod=mysqli_fetch_array($pro)) {
		    			if ($prod[4]==1 ) {
		    				$estado="Si";
		    			}
		    			if ($prod[4]==0 ) {
		    				$estado="No";
		    			}
		    			echo '
		    			<div class="filaB"  >
				    			
					    		<div class="filaNombre">
					    			'.$prod[1] .'
					    		</div>
					    		<div class="filaClave">
					    			'.$prod[6] .'
					    		</div>
					    		<div class="filaNumeros">
					    			$'.$prod[3] .'
					    		</div>
					    		<div class="filaNumeros">
					    			$'.$prod[2] .'
					    		</div>
					    		<div class="filaTelefono">
					    				<a href="controler/dp.php?idPro='.$prod[0] .'">Desactivar</a>
					    		</div>
					    		<div class="filaNumeros">
					    			<a href="controler/ap.php?idPro='.$prod[1] .'">Activar</a>
					    		</div>
					    			<div class="filaNumeros">';
					    		$nombre_fichero = 'productos2/'.$prod[1].'.jpg';
					    		if (file_exists($nombre_fichero)) {
                                    echo '<a href="productos2/'.$prod[1].'.jpg" target="_blank" >Imagen</a>';
                                } else {
                                    echo '<a href="productos2/0.png" target="_blank" >Imagen</a>';
                                }
		    						
					    		echo' </div>
					    	
					    		<div class="filaNumeros">
					    			'.$estado .'
					    		</div>
					    		
					    		<div class="filaMedioLargo">
					    			Descripcion: '.$prod[8] .'
					    		</div>
					    	</div>';
					    	$sumaSuc=$sumaSuc+($prod[7]*$prod[3]);
		    		}
		    		
				   
		    	 ?>
    </div>
    <style>
	    #invert{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	margin-left: 5%;
	    	font-size: 1.2em;
	    }
	      #invert2{
	    	color: rgba(0,0,0,.6);
	    	margin-top: .5em;
	    	padding: .3em 1%;
	    	background: rgba(0,0,0,.1);
	    	
	    	font-size: 1.4em;
	    }
		.ipt-alm{
			width: 60%;
			display: inline-block;
			border: none;
			border-radius: .2em;
		}
		.btn-alm{
			border: none;
			background: grey;
			color: white;
			width: 30%;
			display: inline-block;
		}
		textarea{
			font-family: font;
		}
    </style>
</body>
</html>
<?php 
		}
	else{
		header("Location: administrador.php");
	}
 ?>